#! /usr/bin/make -f

# debian/rules
# Part of Debian ‘comixcursors’ package.
#
# Copyright © 2010–2017 Ben Finney <bignose@debian.org>
# This is free software; you may copy, modify and/or distribute this work
# under the terms of the GNU General Public License, version 3 or later.
# No warranty expressed or implied.
# See the file ‘/usr/share/common-licenses/GPL-3’ for details.

PACKAGE_BUILDDIR = "$(CURDIR)/debian/build"
INSTALLDIR = "$(CURDIR)/debian/tmp"
ICONS_DIR = usr/share/icons
icons_builddir = "${PACKAGE_BUILDDIR}"/${ICONS_DIR}
icons_wrapdir = "${PACKAGE_BUILDDIR}"/etc/X11/cursors
icons_installdir = "${INSTALLDIR}"/${ICONS_DIR}
icons_destdir = "${DESTDIR}"/${ICONS_DIR}

GENERATED_FILES :=

package_mapping = debian/package-mapping
themeset_packages = $(shell cat ${package_mapping} | cut -d':' -f1)

per_package_files = dirs docs install preinst postinst prerm lintian-overrides

INSTALL = install
WRAP_THEMES = "$(CURDIR)"/debian/wrap-cursor-themes


.PHONY: build
build: build-indep build-arch
	dh $@

.PHONY: build-indep
build-indep: build-stamp
	dh $@

.PHONY: build-arch
build-arch: build-stamp
	dh $@

build-stamp: export DESTDIR="${PACKAGE_BUILDDIR}"
build-stamp:
	$(INSTALL) -d "${PACKAGE_BUILDDIR}"


.PHONY: clean
clean:
	$(RM) -r "${PACKAGE_BUILDDIR}"/*
	$(RM) ${GENERATED_FILES}
	dh $@


.PHONY: install
install: build $(foreach package,${themeset_packages},install-${package}-stamp)
	$(INSTALL) -d "${icons_wrapdir}"
	chmod +x ${WRAP_THEMES}
	PACKAGE_BUILDDIR="${PACKAGE_BUILDDIR}" $(WRAP_THEMES)

	dh $@

GENERATED_FILES += install-*-stamp
GENERATED_FILES += $(foreach suffix,${per_package_files}, \
	$(foreach package_name,$(shell cat ${package_mapping} | cut -d':' -f1), \
		debian/${package_name}.${suffix}))
install-%-stamp: themeset_prefix = $(shell cat ${package_mapping} | grep "^$*:" | cut -d':' -f2)
install-%-stamp:
	echo "* Generating files for package ‘$*’ (themeset prefix “${themeset_prefix}”)"
	for n in ${per_package_files} ; do \
		sed \
			-e "s:@THEMESET_PREFIX@:${themeset_prefix}:" \
			-e "s:@PACKAGE@:$*:" \
			debian/templates/$$n.template > debian/$*.$$n ; \
	done
	touch "$@"


.PHONY: binary-indep
binary-indep: build install
	dh $@

.PHONY: binary-arch
binary-arch: build install

.PHONY: binary
binary: build binary-indep binary-arch

.PHONY: override_dh_auto_clean
override_dh_auto_clean:
#	Skip auto-detection of build tool.
	make clean


.PHONY: get-packaged-orig-source
get-packaged-orig-source:
	$(uscan-download-source) --download-current-version

# Policy §4.9 strongly recommends the ‘get-orig-source’ target:
# “This target is optional, but providing it if possible is a good idea.”
#
# This target is an anomaly: per Policy §4.9, it fetches the *latest*
# upstream source, regardless of this package's version. To fetch the
# upstream source corresponding to this package's *declared* version
# in ‘debian/changelog’, use ‘get-packaged-orig-source’ instead.
.PHONY: get-orig-source
get-orig-source:
	$(uscan-download-source)

get-orig-source get-packaged-orig-source: http_proxy =
get-orig-source get-packaged-orig-source: makefile_dir = $(abspath $(dir $(firstword ${MAKEFILE_LIST})))
get-orig-source get-packaged-orig-source: package_dir = $(abspath $(dir ${makefile_dir}))

define uscan-download-source
	uscan --noconf --verbose \
	    --force-download \
	    --rename \
	    --destdir=$(CURDIR) \
	    --check-dirname-level=0 ${package_dir}
endef # uscan-download-source


.PHONY: override_dh_auto_build
override_dh_auto_build:
#	Skip auto-detection of build tool.
	DESTDIR="${PACKAGE_BUILDDIR}" ./install-all
	rmdir "${icons_builddir}"/default

.PHONY: override_dh_auto_test
override_dh_auto_test:
#	Skip auto-detection of build tool.

.PHONY: override_dh_auto_install
override_dh_auto_install:
#	Skip auto-detection of build tool.
	$(INSTALL) -d "${INSTALLDIR}"
	cp -a "${PACKAGE_BUILDDIR}"/. "${INSTALLDIR}"/.

.PHONY: override_dh_installchangelogs
override_dh_installchangelogs:
	dh_installchangelogs NEWS


# Local variables:
# mode: makefile
# coding: utf-8
# End:
# vim: filetype=make fileencoding=utf-8 :
